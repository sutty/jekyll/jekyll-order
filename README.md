# Numeric order for posts

Enables prioritization of posts by numeric order and date.

## Installation

Add this line to your application's Gemfile:

```ruby
group :jekyll_plugins do
  gem 'jekyll-order'
end
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-order

## Usage

### Configuration

Add the plugin to the plugins array in `_config.yml`:

```yaml
plugins:
- jekyll-order
```

### In pages and posts

Add the `order` integer to posts:

```yaml
---
title: An article
order: 1
---

Content
```

When the site is generated, the `site.posts` liquid variable will be
ordered by the numeric value.  Higher numbers go on top.  If the value
is repeated, the date is used as secondary ordering factor.

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake test` to run the tests. You can also run `bin/console`
for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and tags, and push the
`.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-order>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-order project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).
