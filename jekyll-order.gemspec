# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll/order/version'

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-order'
  spec.version       = Jekyll::Order::VERSION
  spec.authors       = ['f']
  spec.email         = ['f@sutty.nl']

  spec.summary       = 'Jekyll plugin for numeric ordering of posts'
  spec.description   = 'Allows to reorder post based on numeric order along with date'
  spec.homepage = 'https://0xacab.org/sutty/jekyll/jekyll-order'
  spec.license  = 'GPL-3.0'

  spec.metadata['allowed_push_host'] = 'https://rubygems.org/'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rubocop', '~> 0.76'
end
