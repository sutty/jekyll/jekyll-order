begin
  require 'jekyll-archives'

  module Jekyll
    module Archives
      module ArchiveDecorator
        def self.included(base)
          base.class_eval do
            def posts
              @ordered_posts ||= @posts.sort_by do |post|
                [post.data.fetch('order', @posts.size + 1),
                post.data['date']]
              end.reverse
            end
          end
        end
      end
    end
  end

  Jekyll::Archives::Archive.include Jekyll::Archives::ArchiveDecorator
rescue LoadError
  puts 'no existe jekyll-archive'
end
