# frozen_string_literal: true

require 'jekyll/order/version'

module Jekyll
  module Order
    class Error < StandardError; end
    # Your code goes here...
  end
end
