# frozen_string_literal: true

module Jekyll
  module Filters
    module Order
      def order(input)
        return input unless input.respond_to? :sort_by

        input.sort_by do |post|
          case post
          when Jekyll::Drops::DocumentDrop, Hash
            [post['order'].to_i, post['date']]
          when Jekyll::Document, Jekyll::Page
            [post.data['order'].to_i, post.data['date']]
          else
            Jekyll.logger.warn 'Order:', "Not a post: #{post.inspect}"
            [0, nil]
          end
        end.reverse
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::Filters::Order)
