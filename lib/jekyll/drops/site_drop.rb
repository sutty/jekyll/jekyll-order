# frozen_string_literal: true

require 'jekyll/drops/site_drop'

module Jekyll
  module Drops
    # Redefine the site.posts drop so they're ordered by order first and
    # date second
    class SiteDrop
      def posts
        @site_posts ||= @obj.posts.docs.sort_by do |post|
          [post.data.dig('order').to_i, post.data['date']]
        end.reverse
      end
    end
  end
end
