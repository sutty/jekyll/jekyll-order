gem := $(notdir $(PWD))

all: build publish

build:
	echo $(gem)
	chmod -R o=g .
	gem build $(gem).gemspec

publish:
	cloak view rubygems | xargs -rI {} gem push --otp {} *.gem
	mkdir -p published/
	mv *.gem published/
